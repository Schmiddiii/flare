option(
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development',
    'screenshot',
    'screenshot-setup'
  ],
  value: 'default',
  description: 'The build profile for Flare. One of "default" or "development" or "screenshot".'
)
